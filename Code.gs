function onChange(e) {
  var ss = SpreadsheetApp.getActiveSpreadsheet();
  var sh = ss.getSheetByName('history');
  var lastRow = sh.getLastRow();
  var lastID = sh.getRange("C"+lastRow).getValue();
  
  sh.getRange("D"+lastRow).setValue((new Date()).toString());
  
  var columnsToChange = [["mentors",19],['mentors',20],
                         ['mentors_translatable',2],['mentors_translatable',3],
                         ['students',12],['students',13],
                         ['mentors_translatable_translations',13],['mentors_translatable_translations',14],
                         ['mentors_topics',2],['mentors_topics',3],
                         ['mentors_courses',5],['mentors_courses',6],
                         ['scheduled_courses',4],['scheduled_courses',5],
                         ['topics_translations',6],['topics_translations',7],
                         ['scheduled_entities',7],['scheduled_entities',8],
                         ['scheduled_entities',13],['scheduled_entities',14],
                         ['topics',7],['topics',8],
                         ['courses',7],['courses',8],
                         ['scheduled_hours',5],['scheduled_hours',6]];
  var analyticsSH = SpreadsheetApp.openById(lastID);
  Logger.log(analyticsSH);
  for (var column = 0; column < columnsToChange.length; column++){
    changeColumn(analyticsSH,columnsToChange[column][0],columnsToChange[column][1]);
  }
                         
}
  
function changeColumn(spreadsheet,sheetName,columnNo){
  var sh = spreadsheet.getSheetByName(sheetName);
  var lastRow = sh.getLastRow();
  var excelDate;
  var range = sh.getRange(2,columnNo,lastRow);
  var rangeVals = range.getValues();
  for (var r = 0; r < rangeVals.length; r++){
    //Logger.log(r);
    try{
      excelDate = getExcelDate(rangeVals[r][0].toString());
      rangeVals[r][0] = excelDate;
    }
    catch(e){
    }
  }
  range.setValues(rangeVals);
}

function getExcelDate(str){
  var monthHash = {Jan: 1, Feb: 2, Mar: 3, Apr: 4, May: 5, Jun: 6, Jul: 7, Aug: 8, Sep: 9, Oct: 10, Nov: 11, Dec: 12};
  var words = str.split(" ");
  var month = monthHash[words[1]];
  var day = parseInt(words[2],10);
  var yr = parseInt(words[3],10);
  
  var time = words[4].split(":");
  
  var hr = parseInt(time[0],10);
  var min = parseInt(time[1],10);
  var sec = parseInt(time[2],10);
  
  var excelEpoch = new Date(1899,11,31,24,23,18); // this is actually jan 1, 1900 believe it or not
  var strTime = new Date(yr,month-1,day,hr,min,sec);
  var parsed = Date.parse(str);
  return (Math.abs(strTime - excelEpoch))/1000/60/60/24+2;
}