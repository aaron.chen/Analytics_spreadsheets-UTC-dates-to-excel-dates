# Analytics_spreadsheets-UTC-dates-to-Excel-Dates

Note that this is a Google script bound to the "Analytics_spreadsheets/production/Production Analytics_spreadsheets History" google sheet.

You shouldn't need to touch this script or run it. A zap creates a new line in the "Production Analytics_spreadsheets History" sheet once it detects that a new Analytics spreadsheet (database) sheet has been created in this folder.

This script gets triggered once the the "Production Analytics_spreadsheets History" sheet is changed (should only be changed by the zap).

In the script there is a list of columns for which it is going to convert the UTC style dates (strings) into excel style dates (numbers). The columns are listed by what sheet they're contained in, and the column number.

Provided the columns' entries are UTC style strings, the script breaks down the words (separated by a space) to determine the month, year, date, etc. It uses this information to determine the number of seconds since January 1, 1900 (this is what an excel date is). It then replaces the original UTC value.

As zaps run every 10 minutes, and it takes about ~30 second for google to detect there's been a change in the sheet, the sheets will not get changed immediately.
